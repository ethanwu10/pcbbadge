EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D1
U 1 1 5B787DFE
P 2500 1750
F 0 "D1" H 2500 1650 50  0000 C CNN
F 1 "LED" H 2500 1850 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 2500 1750 50  0001 C CNN
F 3 "~" H 2500 1750 50  0001 C CNN
	1    2500 1750
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D4
U 1 1 5B78897F
P 3100 1750
F 0 "D4" H 3100 1650 50  0000 C CNN
F 1 "LED" H 3100 1850 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 3100 1750 50  0001 C CNN
F 3 "~" H 3100 1750 50  0001 C CNN
	1    3100 1750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5B788ADE
P 2750 2700
F 0 "R1" V 2650 2700 50  0000 C CNN
F 1 "1k" V 2750 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2680 2700 50  0001 C CNN
F 3 "~" H 2750 2700 50  0001 C CNN
	1    2750 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D6
U 1 1 5B78898B
P 3100 2450
F 0 "D6" H 3100 2350 50  0000 C CNN
F 1 "LED" H 3100 2550 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 3100 2450 50  0001 C CNN
F 3 "~" H 3100 2450 50  0001 C CNN
	1    3100 2450
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D5
U 1 1 5B788985
P 3100 2100
F 0 "D5" H 3100 2000 50  0000 C CNN
F 1 "LED" H 3100 2200 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 3100 2100 50  0001 C CNN
F 3 "~" H 3100 2100 50  0001 C CNN
	1    3100 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D3
U 1 1 5B787EF6
P 2500 2450
F 0 "D3" H 2500 2350 50  0000 C CNN
F 1 "LED" H 2500 2550 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 2500 2450 50  0001 C CNN
F 3 "~" H 2500 2450 50  0001 C CNN
	1    2500 2450
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D2
U 1 1 5B787EC4
P 2500 2100
F 0 "D2" H 2500 2000 50  0000 C CNN
F 1 "LED" H 2500 2200 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 2500 2100 50  0001 C CNN
F 3 "~" H 2500 2100 50  0001 C CNN
	1    2500 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5B788ECE
P 3350 2700
F 0 "R2" V 3250 2700 50  0000 C CNN
F 1 "1k" V 3350 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 3280 2700 50  0001 C CNN
F 3 "~" H 3350 2700 50  0001 C CNN
	1    3350 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 1600 2350 1750
Wire Wire Line
	2350 1600 2950 1600
Wire Wire Line
	2950 1600 2950 1750
Wire Wire Line
	2350 1950 2350 2100
Connection ~ 2350 1950
Wire Wire Line
	2350 1950 2950 1950
Wire Wire Line
	2950 1950 2950 2100
Wire Wire Line
	2950 2450 2950 2300
Wire Wire Line
	2350 2300 2350 2450
Connection ~ 2350 2300
Wire Wire Line
	2350 2300 2950 2300
Wire Wire Line
	2650 1750 2750 1750
Wire Wire Line
	2750 1750 2750 2100
Wire Wire Line
	2650 2100 2750 2100
Connection ~ 2750 2100
Wire Wire Line
	2750 2100 2750 2450
Wire Wire Line
	2650 2450 2750 2450
Wire Wire Line
	2750 2850 2750 3100
Wire Wire Line
	3250 1750 3350 1750
Wire Wire Line
	3350 1750 3350 2100
Wire Wire Line
	3250 2100 3350 2100
Connection ~ 3350 2100
Wire Wire Line
	3350 2100 3350 2450
Wire Wire Line
	3250 2450 3350 2450
Wire Wire Line
	3350 2850 3350 3100
$Comp
L MCU_Microchip_ATtiny:ATtiny85V-10SU U1
U 1 1 5B78B8B8
P 2000 4500
F 0 "U1" H 1470 4546 50  0000 R CNN
F 1 "ATtiny25V-10SU" H 1470 4455 50  0000 R CNN
F 2 "Package_SO:SOIJ-8_5.3x5.3mm_P1.27mm" H 2000 4500 50  0001 C CIN
F 3 "" H 2000 4500 50  0001 C CNN
	1    2000 4500
	1    0    0    -1  
$EndComp
Text Label 2000 1600 0    50   ~ 0
LM_X1
Text Label 2000 1950 0    50   ~ 0
LM_X2
Text Label 2000 2300 0    50   ~ 0
LM_X3
Text Label 2750 3100 1    50   ~ 0
LM_Y1
Text Label 3350 3100 1    50   ~ 0
LM_Y2
Text Label 2900 4200 2    50   ~ 0
LM_X1
Text Label 2900 4300 2    50   ~ 0
LM_X2
Text Label 2900 4400 2    50   ~ 0
LM_X3
Wire Wire Line
	2600 4500 2900 4500
Text Label 2900 4500 2    50   ~ 0
LM_Y1
Wire Wire Line
	2600 4600 2900 4600
Text Label 2900 4600 2    50   ~ 0
LM_Y2
Text Label 2950 4700 2    50   ~ 0
M_RESET
$Comp
L power:GND #PWR02
U 1 1 5B7954A1
P 2000 5100
F 0 "#PWR02" H 2000 4850 50  0001 C CNN
F 1 "GND" H 2005 4927 50  0000 C CNN
F 2 "" H 2000 5100 50  0001 C CNN
F 3 "" H 2000 5100 50  0001 C CNN
	1    2000 5100
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR01
U 1 1 5B795504
P 2000 3900
F 0 "#PWR01" H 2000 3750 50  0001 C CNN
F 1 "+BATT" H 2015 4073 50  0000 C CNN
F 2 "" H 2000 3900 50  0001 C CNN
F 3 "" H 2000 3900 50  0001 C CNN
	1    2000 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5B7959E8
P 3550 4350
F 0 "J1" H 3600 4667 50  0000 C CNN
F 1 "ICSP" H 3600 4576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 3550 4350 50  0001 C CNN
F 3 "~" H 3550 4350 50  0001 C CNN
	1    3550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 4700 3250 4450
Wire Wire Line
	2600 4700 3250 4700
$Comp
L power:GND #PWR04
U 1 1 5B796D50
P 3900 4500
F 0 "#PWR04" H 3900 4250 50  0001 C CNN
F 1 "GND" H 3905 4327 50  0000 C CNN
F 2 "" H 3900 4500 50  0001 C CNN
F 3 "" H 3900 4500 50  0001 C CNN
	1    3900 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR03
U 1 1 5B796D9A
P 3900 4200
F 0 "#PWR03" H 3900 4050 50  0001 C CNN
F 1 "+BATT" H 3915 4373 50  0000 C CNN
F 2 "" H 3900 4200 50  0001 C CNN
F 3 "" H 3900 4200 50  0001 C CNN
	1    3900 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4200 3900 4250
Wire Wire Line
	3900 4250 3850 4250
Wire Wire Line
	3850 4450 3900 4450
Wire Wire Line
	3900 4450 3900 4500
Wire Wire Line
	3150 4300 3150 4250
Wire Wire Line
	3150 4250 3350 4250
Wire Wire Line
	2600 4300 3150 4300
Wire Wire Line
	3200 4400 3200 4350
Wire Wire Line
	3200 4350 3350 4350
Wire Wire Line
	2600 4400 3200 4400
Wire Wire Line
	3100 4200 3100 3900
Wire Wire Line
	3100 3900 4100 3900
Wire Wire Line
	4100 3900 4100 4350
Wire Wire Line
	4100 4350 3850 4350
Wire Wire Line
	2600 4200 3100 4200
Wire Wire Line
	3250 4450 3350 4450
$Comp
L Device:Battery_Cell BT1
U 1 1 5B79CFE7
P 4950 2750
F 0 "BT1" H 5068 2846 50  0000 L CNN
F 1 "CR2032" H 5068 2755 50  0000 L CNN
F 2 "Custom:BK_913" V 4950 2810 50  0001 C CNN
F 3 "~" V 4950 2810 50  0001 C CNN
	1    4950 2750
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR05
U 1 1 5B7A113B
P 4950 2450
F 0 "#PWR05" H 4950 2300 50  0001 C CNN
F 1 "+BATT" H 4965 2623 50  0000 C CNN
F 2 "" H 4950 2450 50  0001 C CNN
F 3 "" H 4950 2450 50  0001 C CNN
	1    4950 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2550 4950 2450
$Comp
L power:GND #PWR06
U 1 1 5B7A1A33
P 4950 2950
F 0 "#PWR06" H 4950 2700 50  0001 C CNN
F 1 "GND" H 4955 2777 50  0000 C CNN
F 2 "" H 4950 2950 50  0001 C CNN
F 3 "" H 4950 2950 50  0001 C CNN
	1    4950 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2950 4950 2850
$Comp
L Device:C C1
U 1 1 5B7A26F3
P 4650 2700
F 0 "C1" H 4536 2654 50  0000 R CNN
F 1 "20uf" H 4536 2745 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4688 2550 50  0001 C CNN
F 3 "~" H 4650 2700 50  0001 C CNN
	1    4650 2700
	1    0    0    1   
$EndComp
Wire Wire Line
	4650 2550 4950 2550
Connection ~ 4950 2550
Wire Wire Line
	4650 2850 4950 2850
Connection ~ 4950 2850
Wire Wire Line
	2000 1600 2350 1600
Wire Wire Line
	2000 1950 2350 1950
Wire Wire Line
	2000 2300 2350 2300
Connection ~ 2350 1600
Wire Wire Line
	2750 2550 2750 2450
Connection ~ 2750 2450
Wire Wire Line
	3350 2550 3350 2450
Connection ~ 3350 2450
$EndSCHEMATC
